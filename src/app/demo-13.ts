import { load } from './loader-13';
import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo13 implements DemoInterface {

    title = "Unsubscribing for Cleanup";
    items: string[] = [];

    run() {
        let output = document.getElementById('output');
        let button = document.getElementById('button');

        // Observable
        let click = Observable.fromEvent(button, "click");

        function renderMovies(movies) {
            movies.forEach(m => {
                let div = document.createElement("div");
                div.innerText = m.title;
                output.appendChild(div);
            });
        }

        // Observable
        let subscription =
            load("assets/movies.json")  // change to invalid name to see retry logic with unsubscribe in asction! (but comment out .unsubscribe below!)
                .subscribe(
                renderMovies,
                e => {
                    console.log(`error. ${e}`);
                },
                () => this.output(`complete`)
                );

        console.log(subscription);
        subscription.unsubscribe(); // this should unsubscribe /abort 

        click.flatMap(e => load("assets/movies.json"))
            .subscribe(
            renderMovies,
            e => this.output(`error: ${e}`),
            () => this.output(`complete`)
            );
    }

    output(info: any): void {
        console.log(info);
        this.items.push(`x: ${info.x}, y: ${info.y}`);
    }
}