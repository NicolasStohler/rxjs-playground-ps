import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo08 implements DemoInterface {

    title = "Using flatMap to Process Inner Observables";
    items: string[] = [];

    run() {
        let output = document.getElementById('output');
        let button = document.getElementById('button');

        // Observable
        let click = Observable.fromEvent(button, "click");

        function load(url: string) {
            return Observable.create(observer => {
                let xhr = new XMLHttpRequest();

                xhr.addEventListener("load", () => {
                    let data = JSON.parse(xhr.responseText);
                    observer.next(data);
                    observer.complete();
                });

                xhr.open("GET", url);
                xhr.send();
            });
        }

        function renderMovies(movies) {
            movies.forEach(m => {
                let div = document.createElement("div");
                div.innerText = m.title;
                output.appendChild(div);
            });
        }

        // Observer:

        // // this subscribe subscribes to the click event!
        // click.map(e => load("assets/movies.json"))
        //     .subscribe(o => console.log(o));     

        // // flatMap/mergeMap: Maps each value to an Observable, then flattens all of these inner Observables using mergeAll 
        // click.flatMap(e => load("assets/movies.json"))
        //     .subscribe(o => console.log(o));

        // note: this will not load the movies! no subscribe invoked!!!
        //load("assets/movies.json");

        // ...but this does:
        load("assets/movies.json").subscribe(renderMovies);

        click.flatMap(e => load("assets/movies.json"))
            .subscribe(
                renderMovies,
                e => this.output(`error: ${e}`),
                () => this.output(`complete`)
            );
    }

    output(info: any): void {
        console.log(info);
        this.items.push(`x: ${info.x}, y: ${info.y}`);
    }
}