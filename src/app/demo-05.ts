import { DemoInterface } from './demo-interface';

import { Observable } from 'rxjs/Rx';

// better rxjs loading (for production):
// import { Observable } from 'rxjs/Observable';
// import './rxjs-operators'; // ONCE in app.module.ts

export class Demo05 implements DemoInterface {

    title = "Using RxJS Operators";
    items: string[] = [];

    run() {
        let numbers = [1, 5, 10];

        // Observable
        let source = Observable.create(observer => {

            let index = 0;

            let produceValue = () => {
                observer.next(numbers[index]);
                index++; 

                if (index < numbers.length) {
                    setTimeout(produceValue, 300);
                } else {
                    observer.complete();
                }
            }

            produceValue();
        })
        .map(n => n * 2)        // transform result
        .filter(n => n > 4)     // filter: keep those that return true
        ;

        // Observer
        source.subscribe(
            value => this.output(`value: ${value}`),
            e => this.output(`error: ${e}`),
            () => this.output(`complete`)
        );
    }

    output(text: string): void {
        console.log(text);
        this.items.push(text);
    }
}