import { Observable } from 'rxjs/Rx';

export function load(url: string) {
    return Observable.create(observer => {
        let xhr = new XMLHttpRequest();

        xhr.addEventListener("load", () => {
            if (xhr.status === 200) {
                let data = JSON.parse(xhr.responseText);
                observer.next(data);
                observer.complete();
            } else {
                observer.error(xhr.statusText); // will be passed into retryStrategy.return-function!
            }
        });

        xhr.open("GET", url);
        xhr.send();
    })
        // .retry(3)       // retry x times
        .retryWhen(retryStrategy({ attempts: 3, delay: 1500 }))
        ;
}

export function loadWithFetch(url: string) {
    // add lazy loading/defer (QQQ):
    return Observable.defer(() => {
        return Observable.fromPromise(
            fetch(url).then(r => {
                // console.log('ggg');
                if (r.status === 200) {
                    return r.json(); // returns result as promise!
                } else {
                    return Promise.reject(r);
                }
            }
            ))
            //.retryWhen(retryStrategy()); // wrong position: will not retry network op! 
            ;
    }).retryWhen(retryStrategy());
    // no lazy loading (execute even if no subscribe invoked!):
    // return Observable.fromPromise(fetch(url).then(r => r.json()));

}

function retryStrategy({ attempts = 4, delay = 1000 } = {}) {
    return function (errors) {
        //console.log(errors);
        return errors
            .scan((acc, value) => {
                console.log("x:", value);
                acc += 1;
                if (acc < attempts) {
                    return acc;
                } else {
                    throw new Error(value);
                }
            }, 0) // acc start value
            //.takeWhile(acc => acc < attempts) // not needed anymore, all is done in .scan!
            .delay(delay);
    }
}