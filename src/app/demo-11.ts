import { load, loadWithFetch } from './loader';
import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo11 implements DemoInterface {

    title = "Dealing with Errors and Exceptions";
    items: string[] = [];

    run() {
        this.run1();
        // this.run2();
        this.run3();
        this.run4();
        this.run5();
        this.run6();
        this.run7();
    }

    run1() {
        console.log("---1: no errors");
        let source = Observable.create(observer => {
            // 1
            observer.next(1);
            observer.next(2);
        });

        source.subscribe(
            value => console.log(`value: ${value}`),
            error => console.log(`error: ${error}`),
            () => console.log(`completed`)
        );
    }

    run2() {
        console.log("---2: error, no error handler");
        let source = Observable.create(observer => {
            // 1
            observer.next(1);
            observer.next(2);
            // 2 
            observer.error("stop!!!"); // expected error
            // throw new Error("stop ex"); // unexpected error
            observer.next(3);
            observer.complete();
        });

        source.subscribe(
            value => console.log(`value: ${value}`)
        );
    }

    run3() {
        console.log("---3: error, with error handler (stops at error)");
        let source = Observable.create(observer => {
            // 1
            observer.next(1);
            observer.next(2);
            // 2 
            observer.error("stop!!!"); // expected error
            // throw new Error("stop ex"); // unexpected error
            observer.next(3);
            observer.complete();
        });

        source.subscribe(
            value => console.log(`value: ${value}`),
            error => console.log(`error: ${error}`),
            () => console.log(`completed`)
        );
    }

    run4() {
        console.log("---4: merge, no errors");
        let source = Observable.merge(
            Observable.of(1),
            Observable.from([2, 3, 4]),
            Observable.range(5, 4)
        );

        source.subscribe(
            value => console.log(`value: ${value}`),
            error => console.log(`error: ${error}`),
            () => console.log(`completed`)
        );
    }

    run5() {
        console.log("---5: merge, throw");
        let source = Observable.merge(
            Observable.of(1),
            Observable.from([2, 3, 4]),
            Observable.throw("stop!!!"), 
            Observable.range(5, 4) // these will not be produced! (after throw!)
        );

        source.subscribe(
            value => console.log(`value: ${value}`),
            error => console.log(`error: ${error}`),
            () => console.log(`completed`)
        );
    }

    run6() {
        console.log("---6: merge, continue even if error: onErrorResumeNext (ignores error)");
        let source = Observable.onErrorResumeNext(
            Observable.of(1),
            Observable.from([2, 3, 4]),
            Observable.throw("stop!!!"), 
            Observable.range(5, 4) // these will not be produced! (after throw!)
        );

        source.subscribe(
            value => console.log(`value: ${value}`),
            error => console.log(`error: ${error}`),
            () => console.log(`completed`)
        );
    }

    run7() {
        console.log("---7: merge, catch");
        let source = Observable.merge(
            Observable.of(1),
            Observable.from([2, 3, 4]),
            Observable.throw("stop!!!"), 
            Observable.range(5, 4) // these will not be produced! (after throw!)
        )
        .catch(e => {
            console.log(`caught: ${e}`);

            // network error caught, return some cached data maybe???
            return Observable.of(999); 
            
            // or if cache data not available: return Observable.throw(...) to continue 
            // error propagation:
            // return Observable.throw("no network or cache!");
        });

        source.subscribe(
            value => console.log(`value: ${value}`),
            error => console.log(`error: ${error}`),
            () => console.log(`completed`)
        );
    }
}