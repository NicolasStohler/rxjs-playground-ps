import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo07 implements DemoInterface {

    title = "Sending Requests with XmlHttpRequest";
    items: string[] = [];

    run() {
        let output = document.getElementById('output');
        let button = document.getElementById('button');

        // Observable
        let click = Observable.fromEvent(button, "click");

        function load(url: string) {
            let xhr = new XMLHttpRequest();

            xhr.addEventListener("load", () => {
                let movies = JSON.parse(xhr.responseText);
                movies.forEach(m => {
                    let div = document.createElement("div");
                    div.innerText = m.title;
                    output.appendChild(div);
                });
            });

            xhr.open("GET", url);
            xhr.send();
        }

        // Observer
        click.subscribe(
            e => load("assets/movies.json"),
            e => this.output(`error: ${e}`),
                () => this.output(`complete`)
            );
    }

    output(info: any): void {
        console.log(info);
        this.items.push(`x: ${info.x}, y: ${info.y}`);
    }
}