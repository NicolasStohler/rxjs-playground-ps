import { observable } from 'rxjs/symbol/observable';
import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo03 implements DemoInterface {

    title = "Using Observer.create";
    items: string[] = [];

    run() {
        //console.log

        let numbers = [1, 5, 10];
        let source = Observable.create(observer => {

            for (let n of numbers) {

                // if (n === 5) {
                //     observer.error("something went wrong");
                // }

                observer.next(n);
            }

            observer.complete();
        });

        source.subscribe(
            value => this.output(`value: ${value}`),
            e => this.output(`error: ${e}`),
            () => this.output(`complete`)
        );
    }

    output(text: string): void {
        console.log(text);
        this.items.push(text);
    }
}