import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo06 implements DemoInterface {

    title = "Processing Mouse Events";
    items: string[] = [];


    run() {
        let circle = document.getElementById('circle');

        // Observable
        let source = Observable.fromEvent(document, "mousemove")
            .map((e: MouseEvent) => {
                return {
                    x: e.clientX,
                    y: e.clientY
                }
            })
            .filter(value => value.x < 250)
            .delay(300)
            ;

        function onNext(value) {
            // console.log(value, circle.style.left);
            circle.style.left = value.x + "px";
            circle.style.top = value.y + "px";
        }

        // Observer
        source.subscribe(
            v => onNext(v),
            e => this.output(`error: ${e}`),
            () => this.output(`complete`)
        );
    }

    output(info: any): void {
        console.log(info);
        this.items.push(`x: ${info.x}, y: ${info.y}`);
    }


}