import { Demo13 } from './demo-13';
import { Demo12 } from './demo-12';
import { Demo11 } from './demo-11';
import { Demo10 } from './demo-10';
import { Demo09 } from './demo-09';
import { Demo08 } from './demo-08';
import { Demo07 } from './demo-07';
import { Demo06 } from './demo-06';
import { Demo05 } from './demo-05';
import { Demo04 } from './demo-04';
import { Demo03 } from './demo-03';
import { Demo02 } from './demo-02';
import { Demo01 } from './demo-01';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'RxJS';

  sample = new Demo13();

  moduleTitle = '';

  public ngOnInit(): void {
    this.moduleTitle = this.sample.title;
    // let sample = new Demo01();
    // let sample = new Demo02();
    // let sample = new Demo03();

    this.sample.run();
  }
}
