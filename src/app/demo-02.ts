import { DemoInterface } from './demo-interface';
import { Observable, Observer } from 'rxjs/Rx';

export class Demo02 implements DemoInterface {

    run() {
        // "An Easier Observer"

        let numbers = [1, 5, 10];
        let source = Observable.from(numbers);

        source.subscribe(
            value => console.log(`value: ${value}`),
            e => console.log(`error: ${e}`),
            () => console.log(`complete`)
        );
    }
}