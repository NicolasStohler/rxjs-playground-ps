import { load, loadWithFetch } from './loader';
import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo12 implements DemoInterface {

    title = "Revisiting the retryWhen Strategy (loadWithFetch)";
    items: string[] = [];

    run() {
        let output = document.getElementById('output');
        let button = document.getElementById('button');

        // Observable
        let click = Observable.fromEvent(button, "click");

        function renderMovies(movies) {
            movies.forEach(m => {
                let div = document.createElement("div");
                div.innerText = m.title;
                output.appendChild(div);
            });
        }

        // Observer:
        loadWithFetch("assets/moviesNotExisting.json")
            .subscribe(
                renderMovies,
                e => {
                    console.log(`error. ${e}`);
                    console.log("y:", e);
                },
                () => this.output(`complete`)
            );

        click.flatMap(e => loadWithFetch("assets/movies.json"))   
            .subscribe(
            renderMovies,
            e => this.output(`error: ${e}`),
            () => this.output(`complete`)
            );
    }

    output(info: any): void {
        console.log(info);
        this.items.push(`x: ${info.x}, y: ${info.y}`);
    }
}