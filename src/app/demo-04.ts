import { observable } from 'rxjs/symbol/observable';
import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo04 implements DemoInterface {

    title = "Going async with setTimeout";
    items: string[] = [];

    run() {
        let numbers = [1, 5, 10];

        // Observable
        let source = Observable.create(observer => {

            let index = 0;

            let produceValue = () => {
                observer.next(numbers[index]);
                index++;

                if (index < numbers.length) {
                    setTimeout(produceValue, 2000);
                } else {
                    observer.complete();
                }
            }

            produceValue();
        });

        // Observer
        source.subscribe(
            value => this.output(`value: ${value}`),
            e => this.output(`error: ${e}`),
            () => this.output(`complete`)
        );
    }

    output(text: string): void {
        console.log(text);
        this.items.push(text);
    }
}