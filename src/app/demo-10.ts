import { DemoInterface } from './demo-interface';
import { Observable } from 'rxjs/Rx';

export class Demo10 implements DemoInterface {

    title = "Using fetch and Promises";
    items: string[] = [];

    run() {
        let output = document.getElementById('output');
        let button = document.getElementById('button');

        // Observable
        let click = Observable.fromEvent(button, "click");

        function load(url: string) {
            return Observable.create(observer => {
                let xhr = new XMLHttpRequest();

                xhr.addEventListener("load", () => {
                    if (xhr.status === 200) {
                        let data = JSON.parse(xhr.responseText);
                        observer.next(data);
                        observer.complete();
                    } else {
                        observer.error(xhr.statusText); // will be passed into retryStrategy.return-function!
                    }
                });

                xhr.open("GET", url);
                xhr.send();
            })
                // .retry(3)       // retry x times
                .retryWhen(retryStrategy({attempts: 3, delay: 1500}))
                ;
        }

        function loadWithFetch(url: string) {
            // add lazy loading/defer (QQQ):
            return Observable.defer(() => {
                return Observable.fromPromise(fetch(url).then(r => r.json()));
            });
            // no lazy loading (execute even if no subscribe invoked!):
            // return Observable.fromPromise(fetch(url).then(r => r.json()));

        }

        function retryStrategy({attempts = 4, delay = 1000}) {
            return function (errors) {
                //console.log(errors);
                return errors
                    .scan((acc, value) => {
                        console.log(acc, value);
                        return acc + 1;
                    }, 0) // acc start value
                    .takeWhile(acc => acc < attempts)
                    .delay(delay);
            }
        }

        function renderMovies(movies) {
            movies.forEach(m => {
                let div = document.createElement("div");
                div.innerText = m.title;
                output.appendChild(div);
            });
        }

        // Observer:
        // load("assets/movies.json").subscribe(renderMovies);

        // note the difference: this will make the webrequest right away!
        // before change QQQ
        loadWithFetch("assets/movies.json"); 
            //.subscribe(renderMovies);

        // click.flatMap(e => load("assets/movies.json"))

        // click.flatMap(e => load("assets/movies.json"))   
        click.flatMap(e => loadWithFetch("assets/movies.json"))   
            .subscribe(
            renderMovies,
            e => this.output(`error: ${e}`),
            () => this.output(`complete`)
            );
    }

    output(info: any): void {
        console.log(info);
        this.items.push(`x: ${info.x}, y: ${info.y}`);
    }
}